import { Component } from '@angular/core';
import {Filter} from "./models/filter.interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todos = [];
  completedState: string = '';
  filter: Filter = {
    search: '',
    completed: ''
  };

  addTodo (e) {
    this.todos = e;
  }

  removeTodo (todo) {
    const index = this.todos.indexOf(todo);
    this.todos = [... this.todos.slice(0,index),
      ... this.todos.slice(index+1, this.todos.length)];
  }

  updateFilter (e) {
    this.filter = e;
  }
}
