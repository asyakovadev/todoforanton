///<reference path="pipes/completed.filter.ts"/>
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import {SearchFilterPipe} from "./pipes/search.filter";
import {CompletedFilterPipe} from "./pipes/completed.filter";
import { CreateTodoComponent } from './components/create-todo/create-todo.component';
import { TodoFilterComponent } from './components/todo-filter/todo-filter.component';
import { TodoComponent } from './components/todo/todo.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchFilterPipe,
    CompletedFilterPipe,
    CreateTodoComponent,
    TodoFilterComponent,
    TodoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
