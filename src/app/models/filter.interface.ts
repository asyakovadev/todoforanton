export interface Filter {
  search: string;
  completed: string;
}
