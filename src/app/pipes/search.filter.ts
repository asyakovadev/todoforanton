import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
  transform(items: any[], fields: any): any[] {
    if (!items) return [];
    return items.filter((todo) => {
      let returnItem = false;
      for (let property in fields) {
        if (fields.hasOwnProperty(property)) {
          if (todo[property].fullMatch) {
            returnItem = (todo[property].toString() === fields[property].value.toString());
          } else {
            returnItem = (todo[property].toString().indexOf(fields[property].value.toString()) > -1);
          }
          if (!returnItem) break;
        }
      }
      return returnItem
    });
  }
}
