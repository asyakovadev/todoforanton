import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'completedfilter',
  pure: false
})

@Injectable()
export class CompletedFilterPipe implements PipeTransform {
  transform(items: any[], field: string, value: string): any[] {
    if (!items) return [];
    return items.filter((it) => {
      if(value === '') return it;
      return (value === it[field])
  });
  }
}
