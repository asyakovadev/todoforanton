import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo} from "../../models/todo.interface";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input() todo: Todo;
  @Input() i: number;
  @Output() taskRemoved = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  removeTodo () {
    this.taskRemoved.emit(this.todo);
  }

}
