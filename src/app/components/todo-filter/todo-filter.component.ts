import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Filter} from "../../models/filter.interface";

@Component({
  selector: 'app-todo-filter',
  templateUrl: './todo-filter.component.html',
  styleUrls: ['./todo-filter.component.css']
})
export class TodoFilterComponent implements OnInit {

  @Input() completedState: string;
  @Output() private _filtersUpdated = new EventEmitter<Filter>();
  search: string = '';

  constructor() { }

  ngOnInit() {
  }

  updateFilters () {
    this._filtersUpdated.emit({
      search: this.search,
      completed: this.completedState
    })
  }


}
