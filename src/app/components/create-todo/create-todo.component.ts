import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Todo } from '../../models/todo.interface';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  newTodo: string = '';
  @Input() todos: Todo[];
  @Output() taskAdded = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onKeydown(event) {
    if (event.key === "Enter") {
      this.addTodo();
    }
  }

  addTodo() {
    this.todos=[...this.todos, { title: this.newTodo, completed: false }];
    this.newTodo = '';
    this.taskAdded.emit(this.todos);
  }
}
